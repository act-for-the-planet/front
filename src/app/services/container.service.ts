import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UrlService } from './url.service';

@Injectable({
  providedIn: 'root'
})
export class ContainerService {
  
  constructor(private http: HttpClient, private urlService: UrlService) { }
  getContainers(): Observable<any> {
    return this.http.get(`${this.urlService.getUrl()}container`, this.urlService.getHttpOptions());
  }
}
