import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlService } from './url.service';

@Injectable({
  providedIn: 'root'
})
export class TrashService {

  constructor(private http: HttpClient, private urlService: UrlService) { }
  getTrashs(): Observable<any> {
    return this.http.get(`${this.urlService.getUrl()}trash`, this.urlService.getHttpOptions());
  }
}
