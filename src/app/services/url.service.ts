import { Injectable } from '@angular/core';
import setting from '../../assets/setting.json';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  private httpOptions: any;
  constructor() {
    this.httpOptions = {
      headers: new HttpHeaders(setting.headers),
    };
   }

  getUrl(): string{
    return setting[setting.deploy];
  }

  getHttpOptions(): any {
    return this.httpOptions;
  }

  getDeployStatus(): string {
    return setting.deploy;
  }
}
