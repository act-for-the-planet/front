import { Injectable } from '@angular/core';
import { Utilisateur } from '../interfaces/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  public login(user: Utilisateur){
    localStorage.setItem('ACCESS_TOKEN', "access_token");
  }
  public logout(){
    localStorage.removeItem('ACCESS_TOKEN');
  }
  public isLogin(){
    return localStorage.getItem('ACCESS_TOKEN') !== null;
  }
}
