import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { AdminComponent } from './admin/admin.component';
import { ParcComponent } from './parc/parc.component';
import { PlanComponent } from './plan/plan.component';
import { StatsComponent } from './stats/stats.component';
import { EditComponent } from './edit/edit.component';
import { HelpComponent } from './help/help.component';
import { CardTrashComponent } from './card-trash/card-trash.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    ConnexionComponent,
    AdminComponent,
    ParcComponent,
    PlanComponent,
    StatsComponent,
    EditComponent,
    HelpComponent,
    CardTrashComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
