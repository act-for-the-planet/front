import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CardTrashDTO } from '../interfaces/dto/card-trash-dto';
import { ContainerService } from '../services/container.service';
import { TrashService } from '../services/trash.service';

@Component({
  selector: 'app-parc',
  templateUrl: './parc.component.html',
  styleUrls: ['./parc.component.scss']
})
export class ParcComponent implements OnInit {

  cardTrashs:CardTrashDTO[] = [];
  constructor(private trashService:TrashService, private containerService: ContainerService) {
     this.getTrashsAndContainers();
   }

  ngOnInit(): void {
    
  }

  getTrashsAndContainers(){
    this.containerService.getContainers().subscribe(
      (dataContainer: any) => {
        const containers = dataContainer;
        this.trashService.getTrashs().subscribe(
          (dataTrash:any) => {
            const trashs = dataTrash;
            trashs.forEach(trash => {
              let newCardTrash = {
                name: trash.name,
                id: trash.id_trash,
                bacs: []
              }
              let containersOnTrash = containers.filter(container => container.id_trash == trash.id_trash);
              containersOnTrash.forEach(cot => {
                const bac = {id:cot.id_container, filling:cot.filling, treshold:cot.treshold, profondeur:cot.profondeur}
                newCardTrash.bacs.push(bac);
              });
              this.cardTrashs.push(newCardTrash);
            });
          }
        );
      }
    );
  }
}
