import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTrashComponent } from './card-trash.component';

describe('CardTrashComponent', () => {
  let component: CardTrashComponent;
  let fixture: ComponentFixture<CardTrashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardTrashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTrashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
