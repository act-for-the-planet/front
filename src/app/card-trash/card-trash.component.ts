import { Component, Input, OnInit } from '@angular/core';
import { CardTrashDTO } from '../interfaces/dto/card-trash-dto';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-card-trash',
  templateUrl: './card-trash.component.html',
  styleUrls: ['./card-trash.component.scss']
})

export class CardTrashComponent implements OnInit {
  // On récupère le DTO CardTrash
  @Input() trashs:CardTrashDTO;
  authorization: boolean;
  // constante pour calculer la hauteur du remplissage
  private heightDiv:number = 94;
  colorBac = {green:"rgb(0, 135, 27)",orange:"rgb(255, 140, 0)",red:"rgb(200,0,0)"}
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authorization = this.authService.isLogin();
    console.log(this.trashs);
  }

  drawBac(indexBac:number): string{
    if(this.trashs.bacs.length > 0){
      const niveau = (this.trashs.bacs[indexBac].filling / this.trashs.bacs[indexBac].profondeur) * 100;
      let rapportNiveau = (this.heightDiv * niveau) / 100;
      let posY: number = 16 + (this.heightDiv - rapportNiveau);
      let color:string = this.colorBac.green;
      switch(true){
        case (niveau > 90 ):
          color = this.colorBac.red;
          break;
        case (niveau > 70):
          color = this.colorBac.orange;
          break;
      }
      return "transform:translateY(" + posY + "px);height:" + rapportNiveau + "px;background-color:" + color + ";"
    }
    return "";
  }

  getFillingTauxByBac(indexBac:number){
    if(indexBac < this.trashs.bacs.length){
      const taux = (this.trashs.bacs[indexBac].filling / this.trashs.bacs[indexBac].profondeur) * 100;
      return taux.toFixed(0).toString() + " %";
    }
  }

  isAlert(indexBac:number){
    if(indexBac < this.trashs.bacs.length){
      let seuil = 0;
      if(this.trashs.bacs[indexBac].treshold == "") {
        seuil = this.trashs.bacs[indexBac].profondeur * 0.9;
      }
      if(this.trashs.bacs[indexBac].filling >= seuil){
        return true;
      }
    } 
    return false;
  }
}
