import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  authorization: boolean = false;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authorization = this.authService.isLogin();
  }

}
