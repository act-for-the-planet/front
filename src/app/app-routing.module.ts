import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './auth.guard';
import { ConnexionComponent } from './connexion/connexion.component';
import { EditComponent } from './edit/edit.component';
import { HelpComponent } from './help/help.component';
import { ParcComponent } from './parc/parc.component';
import { PlanComponent } from './plan/plan.component';
import { StatsComponent } from './stats/stats.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'parc'},
  { path: 'connexion', component: ConnexionComponent },
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuard]},
  { path: 'parc', component: ParcComponent },
  { path: 'stats', component: StatsComponent},
  { path: 'plan', component: PlanComponent},
  { path: 'edit', component: EditComponent, canActivate: [AuthGuard]},
  { path: 'help', component: HelpComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
